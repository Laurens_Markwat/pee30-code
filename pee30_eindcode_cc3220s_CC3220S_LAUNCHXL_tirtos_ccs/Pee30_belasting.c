#include <unistd.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <pthread.h>
#include <mqueue.h>
#include <ti/drivers/PWM.h>
#include "ti_drivers_config.h"
#include <ti/drivers/ADC.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include "functies/inc/i2c-lcd.h"
#include <ti/drivers/UART.h>
#include <string.h>
#include <stdlib.h>


#define THREADSTACKSIZE   2048

uint16_t adcValue0;
uint32_t adcValue0MicroVolt;
uint16_t adcValue1;
uint16_t adcValue2;
uint32_t adcValue2MicroVolt;

//Variabelen
float Hui_Ver = 0;
float Hui_Spa_D = 0;
float Hui_Spa_V = 0;
float load = 20;
float adcValue1MicroVolt;
float Stroom = 0.0;
float Stroom_Gem = 0.0;
float Spanning_Gem = 0.0;
float Vermogen = 0.0;
float Verschil_vermogen = 0.0;

//fictieve testwaardes
int rpm = 10000;
float hoeksnelheid;
float Vermogen_set = 2.0;
float Temp_set = 21.0;
int overload_set = 20;

char recievedBuffer[9];

volatile uint8_t overload;
volatile uint8_t overspeed;
volatile uint8_t overheat;
volatile uint8_t noodstopIngedrukt;
volatile uint8_t ready;
//Register 1
volatile uint8_t start = 0;
volatile uint8_t startinit;
//Register 2t/m9
float    belastingRendement;
volatile uint8_t temperatuur;
float    belastingKoppel;
volatile uint16_t maxsnelheid;

typedef enum{neutraal,noodstop, overheat1}huidige_staat;
huidige_staat staat = neutraal;
huidige_staat nood_staat = neutraal;

//pthread_mutex_t mutex1;
typedef struct{
    mqd_t controlQueueHeenTemp;
    mqd_t controlQueueTerugTemp;
    mqd_t controlQueueHeenLCD;
    mqd_t controlQueueTerugLCD;
    I2C_Handle i2cHandle_temp;
}queueEnHandle;


void noodstop_1(uint_least8_t index){
    if(GPIO_read(CONFIG_NOODSTOP_0) == 1){
        staat = neutraal;
    }
    else{
        staat = noodstop;
    }
}

float spanning_meten(void)
{
        ADC_Handle   adc;
        ADC_Params   params;
        int_fast16_t res;

        ADC_Params_init(&params);
        adc = ADC_open(CONFIG_ADC_0, &params);

        GPIO_write(CONFIG_GPIO_0, CONFIG_GPIO_LED_ON);

        if (adc == NULL) {
            while (1);
        }

        int i;
        for(i = 0; i<1000; i++){
        res = ADC_convert(adc, &adcValue0);

        if (res == ADC_STATUS_SUCCESS) {

            adcValue0MicroVolt = adcValue0 * 840.82625;

            Hui_Spa_D = adcValue0 * 0.00084;
            Hui_Spa_V = adcValue0 * 0.00084 * (20/1.4);
            Spanning_Gem = Hui_Spa_V + Spanning_Gem;
        }
        else {
        }
        }
        Spanning_Gem = Spanning_Gem/1000.0;
        ADC_close(adc);
        return (Spanning_Gem);
}

float stroom_meten(void)
{
       ADC_Handle   adc;
       ADC_Params   params;
       int_fast16_t res;

       ADC_Params_init(&params);
       adc = ADC_open(CONFIG_ADC_1, &params);

       if (adc == NULL) {
           while (1);
       }
       int i;
       for(i = 0; i<1000; i++){
           res = ADC_convert(adc, &adcValue1);
           if (res == ADC_STATUS_SUCCESS)
           {

               adcValue1MicroVolt = adcValue1 * 840.82625 * 2.5;
               Stroom = adcValue1MicroVolt / 1000000;
               Stroom -= 2.522;    //Spanning als stroom 0 is
               Stroom *= 10.0;//9.82215;
               Stroom += 0.29161; //Offset
               Stroom_Gem = Stroom + Stroom_Gem;

           }
           else
           {
           }
       }
       Stroom_Gem = Stroom_Gem/1000.0;
       ADC_close(adc);
       return (Stroom_Gem);
}

int array_to_int(char array[]){

    char intarray[9] = {0};
    memset(intarray,0,9);
    intarray[0] = array[2];
    intarray[1] = array[3];
    intarray[2] = array[4];
    intarray[3] = array[5];
    intarray[4] = array[6];
    intarray[5] = array[7];
    intarray[6] = array[8];
    intarray[7] = array[9];
    return(atoi(intarray));
    }

void editVariables(void)
{
    /*
     * Noodstop(n) OverLoad(l) OverSpeed(s) OverHeat(h)
     */
    if (recievedBuffer[0] == '1')
    {
        if (recievedBuffer[1] == 'n')
        {
            noodstopIngedrukt = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'l')
        {
            overload = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 's')
        {
            overspeed = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'h')
        {
            overheat = array_to_int(recievedBuffer);
        }
        /*
         * Start(s) StartInit(i)
         */
    }
    else if (recievedBuffer[0] == '2')
    {
        if (recievedBuffer[1] == 's')
        {
            start = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'i')
        {
            startinit = array_to_int(recievedBuffer);
        }
        /*
         * Rendement(r) Temp(t) Koppel(k) Vermogen(v)
         * MaxTemp(T) MaxSnelheid(S) Vermogen_set(V)
         * Ingesteld Vermogen(P)
         */
    }
    else if (recievedBuffer[0] == '3')
    {
        if (recievedBuffer[1] == 'r')
        {
            belastingRendement = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 't')
        {
            temperatuur = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'k')
        {
            belastingKoppel = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'v')
        {
            Vermogen = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'T')
        {
            Temp_set = array_to_int(recievedBuffer);
        }
        else if (recievedBuffer[1] == 'S')
        {
            maxsnelheid = array_to_int(recievedBuffer);;
        }
        else if (recievedBuffer[1] == 'V')
        {
            Vermogen_set = array_to_int(recievedBuffer);
        }
    }
}

void LCDVermogenInit(void)
{
    lcd_init();
    lcd_clear();

    lcd_put_cur(0, 0);
    char *c = "Actueel P: ";
    lcd_send_string(c);

    lcd_put_cur(1, 0);
    char *d = "Delta   P: ";
    lcd_send_string(d);

    lcd_put_cur(0, 15);
    char *e = "W";
    lcd_send_string(e);
    lcd_put_cur(1, 15);
    lcd_send_string(e);
}

//Displays delta P on LCD takes floating point numbers
void deltaP(float verschil){
    float number = verschil;
    char charValue[5];

    sprintf(&charValue[0], "%.1f", number);

    lcd_put_cur(1, 11);
    lcd_send_string(&charValue[0]);
}

//Displays actual P on LCD takes floating point numbers
void actualP(float huidig){
    float number = huidig;
    char charValue[5];

    sprintf(&charValue[0], "%.1f", number);
    lcd_put_cur(0, 11);
    lcd_send_string(&charValue[0]);
}

void *Thread_Control(void *HandleQueue)
{
    char c = '1';
    char d = '0';
    char sendBuffer;
    sendBuffer = c;
    char recieveBuffer1;
    recieveBuffer1 = d;
    char recieveBuffer2;
    recieveBuffer2 = d;
    queueEnHandle HandleQueues = *(queueEnHandle*) HandleQueue;

    while (1)
    {
        if (recieveBuffer1 == d)
        {
            mq_send(HandleQueues.controlQueueHeenTemp, &sendBuffer, 1, 0);
            mq_receive(HandleQueues.controlQueueTerugTemp, &recieveBuffer1, 1, 0);
        }
        if (recieveBuffer1 == c)
        {

            mq_send(HandleQueues.controlQueueHeenLCD, &sendBuffer, 1, 0);
            mq_receive(HandleQueues.controlQueueTerugLCD, &recieveBuffer2, 1, 0);
            if (recieveBuffer2 == c)
            {
                recieveBuffer1 = d;
                recieveBuffer2 = d;
            }
        }
    }

}

void *Noodstop_led(void *arg0)
{
    while(1){
        if(staat == noodstop){
            noodstopIngedrukt = 1;
            GPIO_write(CONFIG_GPIO_LED_ROOD,1);
            GPIO_write(CONFIG_GPIO_LED_BLAUW, 0);
            usleep(98000); //delay voor noodstop_led wisseling
            GPIO_write(CONFIG_GPIO_LED_ROOD, 0);
            GPIO_write(CONFIG_GPIO_LED_BLAUW, 1);
        }
        else{
            if(start == 1){
                GPIO_write(CONFIG_GPIO_LED_ROOD, 1);
                GPIO_write(CONFIG_GPIO_LED_BLAUW, 0);
            }
            else{
                GPIO_write(CONFIG_GPIO_LED_ROOD, 0);
                GPIO_write(CONFIG_GPIO_LED_BLAUW, 0);
            }
            noodstopIngedrukt = 0;
        }
        usleep(98000);
    }
}

void *Lcd_aansturen(void *HandleQueue)
{
    queueEnHandle HandleQueues = *(queueEnHandle*) HandleQueue;
    char c = '1';
    char d = '0';
    char send = c;
    char recieved = d;

    extern I2C_Handle Handle;
    Handle = HandleQueues.i2cHandle_temp;

    LCDVermogenInit();
    actualP(Vermogen); //variable for input display
    deltaP(Verschil_vermogen);

    while (1)
    {
        mq_receive(HandleQueues.controlQueueHeenLCD, &recieved, 1, 0);
        if (recieved == c)
        {
            if(staat != noodstop){
                actualP(Vermogen);
                deltaP(Verschil_vermogen);
                recieved = d;
                sleep(1);
                mq_send(HandleQueues.controlQueueTerugLCD, &send, 1, 0);
            }
            else{
                mq_send(HandleQueues.controlQueueTerugLCD, &send, 1, 0);
            }
        }

    }
}

void *Temp_meting(void *HandleQueue)
{
    queueEnHandle HandleQueues = *(queueEnHandle*) HandleQueue;
    char c = '1';
    char d = '0';
    char recieved = d;
    char send = c;
    while (1)
    {
        mq_receive(HandleQueues.controlQueueHeenTemp, &recieved, 1, 0);
        if (recieved == c)
        {
            bool status = false;
            I2C_Handle Handle;
            Handle = HandleQueues.i2cHandle_temp;
            I2C_Transaction i2cTransaction = { 0 };
            uint8_t readBuffer[2];
            uint8_t writeBuffer[1];
            writeBuffer[0] = 0xC1;
            i2cTransaction.slaveAddress = 0x67;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = 1;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = 2;
            status = I2C_transfer(Handle, &i2cTransaction);
            if (status == false)
            {
                if (i2cTransaction.status == I2C_STATUS_ADDR_NACK)
                {
                    // I2C slave address not acknowledged
                }
            }
            uint8_t MSB, LSB;
            MSB = readBuffer[0];
            LSB = readBuffer[1];
            if ((readBuffer[0] & 0x80) == 0x80)
            {
                LSB = LSB + 127;
                temperatuur = (1024 - ((MSB * 16) + (LSB / 16)));
            }
            else
            {
                temperatuur = ((MSB * 16) + (LSB / 16));
            }
            if(temperatuur > Temp_set){
                nood_staat = overheat1;
                overheat = 1;
            }
            else{
                overheat = 0;
                nood_staat = neutraal;
            }
            recieved = d;
            usleep(10);
            mq_send(HandleQueues.controlQueueTerugTemp, &send, 1, 0);
        }

    }
}
void *Uart_Control(void *arg0)
{
    char sendBuffer[20] = "124\r\n";
        UART_Handle uart;
        UART_Params uartParams;

        /* Call driver init functions */
        UART_init();

        /* Create a UART with data processing off. */
        UART_Params_init(&uartParams);
        uartParams.writeDataMode = UART_DATA_BINARY;
        uartParams.readDataMode = UART_DATA_BINARY;
        uartParams.readReturnMode = UART_RETURN_NEWLINE;
        uartParams.readTimeout = 2000;
        uartParams.baudRate = 9600;

        uart = UART_open(CONFIG_UART_0, &uartParams);

        if (uart == NULL)
        {
            /* UART_open() failed */
            while (1)
                ;
        }
        /* Loop forever echoing */
        while (1)
        {
            int write;
            sprintf(sendBuffer, "1l%d\r\n", overload);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "1s%d\r\n", overspeed);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "1h%d\r\n", overheat);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "1n%d\r\n", noodstopIngedrukt);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "3r%.1f\r\n", belastingRendement);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "3t%d\r\n", temperatuur);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "3k%.1f\r\n", belastingKoppel);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            sprintf(sendBuffer, "3v%.1f\r\n", Vermogen);
            write = UART_write(uart, sendBuffer, sizeof(sendBuffer));
            if (write < 0)
            {
                while (1);
            }
            usleep(200);
            memset(recievedBuffer,0,4);
            int read = UART_read(uart, recievedBuffer, sizeof(recievedBuffer));
            if (read < 0)
            {
                while (1);
            }
            editVariables();
            memset(recievedBuffer,0,4);
        }
}

void *mainThread(void *arg0)
{
    pthread_t           thread0,thread1,thread2;
    pthread_attr_t      attrs;
    int                 retc;
    int                 detachState;

    float      dutycycle = 0;
    uint32_t   dutyValue;

    PWM_Handle pwm1 = NULL;
    PWM_Params params;

    /* Call driver init functions */
    Board_init();
    ADC_init();
    GPIO_init();
    GPIO_write(CONFIG_GPIO_LED_BLAUW, 0);
    GPIO_write(CONFIG_GPIO_LED_ROOD, 0);
    GPIO_enableInt(CONFIG_NOODSTOP_0_CONST);

    /* Call driver init functions. */
    PWM_init();

    //Mqueus
    mq_attr postbus_att;
    postbus_att.mq_maxmsg = 2;
    postbus_att.mq_msgsize = 1;
    postbus_att.mq_flags = 0;

    I2C_init();
    // initialize optional I2C bus parameters
    I2C_Params params1;
    I2C_Params_init(&params1);
    params1.bitRate = I2C_100kHz;

    queueEnHandle doorgeven;
    doorgeven.i2cHandle_temp =  I2C_open(0, &params1);
    doorgeven.controlQueueHeenTemp = mq_open("controlQueueHeentemp", O_RDWR | O_CREAT, 0666, &postbus_att);
    doorgeven.controlQueueTerugTemp = mq_open("controlQueueTerugtemp", O_RDWR | O_CREAT, 0666, &postbus_att);
    doorgeven.controlQueueHeenLCD = mq_open("controlQueueHeenlcd", O_RDWR | O_CREAT, 0666, &postbus_att);
    doorgeven.controlQueueTerugLCD = mq_open("controlQueueTeruglcd", O_RDWR | O_CREAT, 0666, &postbus_att);

    PWM_Params_init(&params);
    params.dutyUnits = PWM_DUTY_FRACTION;
    params.dutyValue = 0;
    params.periodUnits = PWM_PERIOD_HZ;
    params.periodValue = 80e3;
    pwm1 = PWM_open(CONFIG_PWM_0, &params);
    if (pwm1 == NULL) {
        /* CONFIG_PWM_0 did not open */
        while (1);
    }

    PWM_start(pwm1);

    /* Create application threads */
    pthread_attr_init(&attrs);

    detachState = PTHREAD_CREATE_DETACHED;
    /* Set priority and stack size attributes */
    retc = pthread_attr_setdetachstate(&attrs, detachState);
    if (retc != 0) {
        /* pthread_attr_setdetachstate() failed */
        while (1);
    }

    retc |= pthread_attr_setstacksize(&attrs, THREADSTACKSIZE);
    if (retc != 0) {
        /* pthread_attr_setstacksize() failed */
        while (1);
    }

    retc = pthread_create(&thread0, &attrs, Thread_Control, &doorgeven);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1) {}
    }

    retc = pthread_create(&thread0, &attrs, Noodstop_led, NULL);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1) {}
    }

    retc = pthread_create(&thread1, &attrs, Temp_meting, &doorgeven);
    if (retc != 0) {
        /* pthread_create() failed */
        while (1) {}
    }

    retc = pthread_create(&thread2, &attrs, Lcd_aansturen, &doorgeven);
    if (retc != 0) {
         /* pthread_create() failed*/
         while (1) {}
    }

    retc = pthread_create(&thread2, &attrs, Uart_Control, NULL);
    if (retc != 0) {
         /* pthread_create() failed*/
         while (1) {}
    }

    /* Loop forever incrementing the PWM duty */
    while (1) {
        stroom_meten();
        spanning_meten();
        Vermogen = Spanning_Gem*Stroom_Gem;
        belastingRendement = (Vermogen/Vermogen_set)*100.0;
        hoeksnelheid = (2.0*3.14*rpm)/60.0;
        belastingKoppel = Vermogen/hoeksnelheid;
        Verschil_vermogen =  Vermogen_set - Vermogen;
        dutyValue = (uint32_t) (((uint64_t) PWM_DUTY_FRACTION_MAX * dutycycle) / 100);
        PWM_setDuty(pwm1, dutyValue);
        if(overload_set <= Vermogen){
            overload = 1;
        }
        else{
            overload = 0;
        }
        if(rpm >= maxsnelheid){
            overspeed = 1;
        }
        else{
            overspeed = 0;
        }

        if(start == 1){
            if(staat != noodstop){
                if(Verschil_vermogen <= 100 && Verschil_vermogen >= 10){
                    if(dutycycle > 95){
                        dutycycle = 98;
                    }
                    else{
                        dutycycle = dutycycle + 5;
                    }
                }
                else if(Verschil_vermogen <= 10 && Verschil_vermogen >= 1){
                    if(dutycycle > 98){
                        dutycycle = 98;
                    }
                    else{
                        dutycycle = dutycycle + 1;
                    }
                }
                else if(Verschil_vermogen <= 1 && Verschil_vermogen > 0){
                    if(dutycycle > 98){
                        dutycycle = 98;
                    }
                    else{
                        dutycycle = dutycycle + 0.1;
                    }
                }
                else if(Verschil_vermogen >= -100 && Verschil_vermogen <= -10){
                    if(dutycycle < 5){
                        dutycycle = 1;
                    }
                    else{
                        dutycycle = dutycycle - 5;
                    }
                }
                else if(Verschil_vermogen >= -10 && Verschil_vermogen <= -1){
                    if(dutycycle < 2){
                        dutycycle = 1;
                    }
                    else{
                        dutycycle = dutycycle - 1;
                    }
                }
                else if(Verschil_vermogen >= -1 && Verschil_vermogen < 0){
                    if(dutycycle < 1){
                        dutycycle = 1;
                    }
               else{
                 dutycycle = dutycycle - 0.1;
               }
            }
        }
        else{
            dutycycle = 0;
        }
        }
        else{
            dutycycle = 0;
            dutyValue = (uint32_t) (((uint64_t) PWM_DUTY_FRACTION_MAX * dutycycle) / 100);
            PWM_setDuty(pwm1, dutyValue);
        }
        usleep(1);
    }
}
